<?php

/**
 * @file
 * The hosting feature definition for the hosting_drush_backup module.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_drush_backup_hosting_feature() {
   $features['drush_backup'] = array(
      'title' => t('Drush Backups'),
      'description' => t('Provides a task to create site backups with drush archive-dump'),
      'status' => HOSTING_FEATURE_DISABLED,
      'module' => 'hosting_drush_backup',
      'group' => 'experimental',
   );
   return $features;
}
