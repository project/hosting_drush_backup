# Install

Download into the hostmaster site and enable as any other module.

# Usage

## Command line

```
drush @sitename.xyz provision-drush-backup
```

## Interface

* Go to site node
* Click the "Run" action on the "Create Drush Backup" task

# Notes

These backups are not meant to be restored. They are meant as a way for
users to create and access backups in situations where the site directory
is not fully contained in the normal backups created using provision-backup.

# License

Licensed under GPLv2 or later. See LICENSE.txt.
