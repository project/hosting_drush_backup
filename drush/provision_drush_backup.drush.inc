<?php

/**
 * @file
 * Provision/Drush hooks for the provision-drush_backup command.
 */

/**
 * Implements hook_provision_register_autoload().
 *
 * This lets our command be used from a site ocntext correctly.
 */
function provision_drush_backup_provision_register_autoload() {
   static $loaded = FALSE;
   if (!$loaded) {
      $loaded = TRUE;
      $list = drush_commandfile_list();
      $provision_dir = dirname($list['provision']);
      include_once($provision_dir . '/provision.inc');
      include_once($provision_dir . '/provision.service.inc');
      provision_autoload_register_prefix('Provision_', dirname(__FILE__));
   }
}

/**
 * Implements hook_drush_command().
 */
function provision_drush_backup_drush_command() {
   $items['provision-drush_backup'] = array(
      'description' => dt('Create a site backup using drush archive-dump'),
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
      'optional_arguments' => array(
         'backup-file' => dt('The complete filepath of the backup. It will be a gzipped tarball.'),
      ),
      'arguments' => array(
        '@site' => dt('Drush alias of site to backup'),
      ),
      'examples' => array(
         'drush @hostmaster provision-drush-backup @sitename.xyz' => 'Back up the site as defined by the site Drush alias sitename.xyz using archive-dump',
      ),
   );
   return $items;
}

/**
 * Implements drush_hook_COMMAND_validate().
 */
function drush_provision_drush_backup_validate($site = NULL, $backup_file = NULL) {
  // Copied from
  // http://cgit.drupalcode.org/provision/tree/platform/backup.provision.inc
   if (!@drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION)) {
      if (drush_get_option('force', false)) {
         drush_log("clearing error");
         drush_set_context('DRUSH_ERROR_CODE', DRUSH_SUCCESS);
      }
   }
   if (!drush_get_option('installed') && !drush_get_option('force', false)) {
      drush_set_error('PROVISION_DRUPAL_SITE_NOT_FOUND');
   }

   // This is the actual drupal provisioning requirements.
   if (!is_dir(d($site)->platform->server->backup_path)) {
      drush_set_error('PROVISION_BACKUP_PATH_NOT_FOUND');
   }

   if ($backup_file) {
      if (provision_file()->exists($backup_file)->status()) {
         drush_set_error('PROVISION_BACKUP_ALREADY_EXISTS', dt('Back up file @path already exists.', array('@path' => $backup_file)));
      }
      else {
         drush_log(dt('Backing site up to @path.', array('@path' => $backup_file)));
         drush_set_option('backup_file', $backup_file);
      }
   }

   if (!$backup_file) {
      $suggested = drush_hosting_drush_backup_provision_drush_backup_suggest_filename($site);
      drush_set_option('backup_file', $suggested);
   }

}

/**
 * Makeup a file name.
 *
 * Copied from http://cgit.drupalcode.org/provision/tree/platform/backup.provision.inc
 */
function drush_hosting_drush_backup_provision_drush_backup_suggest_filename($site) {
   $suffix = drush_get_option('provision_backup_suffix', '.tar.gz');
   $suggested = d($site)->platform->server->backup_path . '/' . d($site)->uri . '-' . date("Ymd.His", time()) . $suffix;
   // Use format of mysite.com-2008-01-02, if already existing, add number.
   $count = 0;
   while (is_file($suggested)) {
      $count++;
      $suggested = d($site)->platform->server->backup_path . '/' . d($site)->uri . '-' .  date('Ymd.His', time()) . '_' . $count . $suffix;
   }
   return $suggested;
}

/**
 * Implements drush_COMMAND().
 */
function drush_provision_drush_backup($site) {
   drush_log(dt('Start provision-drush_backup command'), 'ok');
   $backup_file = drush_get_option('backup_file');
   $arguments = array(
      '@site' => d($site)->uri,
   );
   $options = array(
     'destination' => $backup_file,
   );
   drush_invoke_process($site, 'archive-dump', $arguments, $options);
}

/**
 * Implements drush_hook_post_COMMAND().
 */
function drush_provision_drush_backup_post_provision_drush_backup($site) {
   // Also from http://cgit.drupalcode.org/provision/tree/platform/backup.provision.inc
   drush_log(dt('Backed up site up to @path.', array('@path' => drush_get_option('backup_file'))), 'success');
   if (d($site)->client_name) {
      $backup_dir = d($site)->server->clients_path . '/' . d($site)->client_name . '/backups';
      provision_file()->create_dir($backup_dir, dt('Client backup directory for @client', array('@client' => d($site)->client_name)), 0750);
      provision_file()->symlink(drush_get_option('backup_file'), $backup_dir . '/' . basename(drush_get_option('backup_file')))
         ->succeed('Created symlink @path to @target')
         ->fail('Could not create symlink @path to @target: @reason');
   }
}

/**
 * Implements drush_hook_COMMAND_rollback().
 */
function drush_provision_drush_backup_provision_drush_backup_rollback($site) {
   // Also from http://cgit.drupalcode.org/provision/tree/platform/backup.provision.inc
   $backup_file = drush_get_option('backup_file');
   if (file_exists($backup_file)) {
      provision_file()->unlink($backup_file)
         ->succeed('Removed stale backup file @path')
         ->fail('Failed deleting backup file @path');
   }
}
