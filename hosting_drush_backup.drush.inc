<?php

/**
 * @file
 * Drush hooks for the hosting_drush_backup module.
 */

/**
 * Implements drush_hook_pre_hosting_task().
 */
function drush_hosting_drush_backup_pre_hosting_task($task) {
  $task = &drush_get_context('HOSTING_TASK');
  // Run the task in the context of the hostmaster
  //$task->args[2] = $task->args[1];
  //$task->args[1] = '@hostmaster';
  if ($task->ref->type == 'site' && $task->task_type == 'drush_backup') {
    $context_name = hosting_context_name($task->ref->nid);
    $task->args[2] = $context_name;
  }
}

/**
 * Implements hook_post_hosting_TASK_task() for drush_backup.
 */
function hosting_drush_backup_post_hosting_drush_backup_task($task, $data) {
  module_load_include('inc', 'hosting_site', 'hosting_site.backups');
  if ($data['context']['backup_file'] && $task->ref->type == 'site') {
    $platform = node_load($task->ref->platform);
    $desc = t('Drush Archive-Dump generated on request - DO NOT USE FOR RESTORES');
    hosting_site_add_backup($task->ref->nid, $platform->web_server,
      $data['context']['backup_file'], $desc, $data['context']['backup_file_size']);
  }
}
